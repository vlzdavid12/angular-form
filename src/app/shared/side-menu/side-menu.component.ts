import {Component} from '@angular/core';


interface MenuItem {
  txt: string;
  ruta: string;
}


@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styles: []
})
export class SideMenuComponent {

  templateMenu: MenuItem[] = [{
    txt: 'Basic',
    ruta: '/template/basics'
  }, {
    txt: 'Dynamics',
    ruta: '/template/dynamics'
  },
    {
      txt: 'Switches',
      ruta: '/template/switches'
    }
  ];

  reactiveMenu: MenuItem[] = [{
    txt: 'Basic',
    ruta: '/reactive/basics'
  }, {
    txt: 'Dynamics',
    ruta: '/reactive/dynamics'
  },
    {
      txt: 'Switches',
      ruta: '/reactive/switches'
    }
  ];

  validationMenu: MenuItem[] = [{
    txt: 'Login',
    ruta: '/auth/login'
  }, {
    txt: 'Register',
    ruta: '/auth/register'
  }];
}
