import {Injectable} from '@angular/core';
import {FormControl, FormGroup, ValidationErrors} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {
  public namePattern: string = '([a-zA-Z]+) ([a-zA-Z]+)';
  public emailPattern: string = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';

  constructor() {
  }

  public nameGetStrider = (control: FormControl): ValidationErrors | null => {
    const value: string = control.value?.trim().toLowerCase();
    if (value === 'strider') {
      return {
        noStrider: true
      };
    }
    return null;
  };

  public inputEquals = (input: string, input2: string) => {
    return (formGroup: FormGroup): ValidationErrors | null => {
      const pass1 = formGroup.get(input)?.value;
      const pass2 = formGroup.get(input2)?.value;

      if (pass1 !== pass2){
        formGroup.get('password2')?.setErrors({noEquals: true});
        return {
          noEquals: true
        };
      }
      return null;
    };
  };

}
