import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AbstractControl, AsyncValidator, FormControl, ValidationErrors} from '@angular/forms';
import {Observable} from 'rxjs';
import {delay, map} from 'rxjs/operators';

interface User {
  id: number;
  email: string;
  username: string;
}


@Injectable({
  providedIn: 'root'
})
export class ValidationEmailService implements AsyncValidator{

  constructor(private http: HttpClient) { }

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    const email = control.value;
    return this.http.get<User[]>(`http://localhost:3000/usuarios?q=${email}`)
      .pipe(
        delay(3000),
        map(
        resp => {
          return (resp.length === 0 ) ? null : {getEmail: true}
        }
      ));
  }
}
