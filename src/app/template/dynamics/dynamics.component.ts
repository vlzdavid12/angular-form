import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';


interface Person {
  name: string;
  favorites: Favorite[];
}

interface Favorite {
  id: number;
  title: string;
}


@Component({
  selector: 'app-dynamics',
  templateUrl: './dynamics.component.html',
  styles: [`.float-end {
    float: right
  }`
  ]
})
export class DynamicsComponent {

  newGame: string = '';

  person: Person =
    {
      name: 'David Valenzuela',
      favorites: [
        {
          id: 1, title: 'Metal Gear',
        },
        {
          id: 2, title: 'DeathStranding',
        }
      ]
    };

  @ViewChild('MyFormDynamic') MyForm!: NgForm;

  nameDynamic(): boolean {
    return this.MyForm?.controls?.name_person?.invalid && this.MyForm?.controls?.name_person?.touched;
  }

  gameDynamic(): boolean {
    return this.MyForm?.controls?.new_game?.value.length >= 4;
  }

  deleteList(index: number): void{
    this.person.favorites.splice(index, 1);
  }


  addGame(): void{
      const newFavorite: Favorite = {
        id: this.person.favorites.length + 1,
        title: this.newGame,
      };
      this.person.favorites.push({...newFavorite});
      this.newGame = '';
  }

  saveForm(): void {

  }


}
