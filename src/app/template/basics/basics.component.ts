import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styles: [`
  .float-end{
    float: right
  }
  `]
})
export class BasicsComponent implements OnInit {

  @ViewChild('MyForm') myForm!: NgForm;

  initForm: {product: string, price: number, exist: number} = {
    product: 'NGX Agular 1900',
    price: 90,
    exist: 2
  };


  constructor() { }

  ngOnInit(): void {
  }


  nameValidate(): boolean{
    return this.myForm?.controls.product?.invalid && this.myForm?.controls.product?.touched;
  }


  priceValidate(): boolean{
    return this.myForm?.controls.price?.value <= 0 && this.myForm?.controls.price?.touched;
  }

  saveForm(): void{
    console.log('Post Correct');
    this.myForm.resetForm({
      price: 0,
      exist: 0
    });
  }

}
