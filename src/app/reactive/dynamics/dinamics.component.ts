import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-dinamics',
  templateUrl: './dinamics.component.html',
  styles: []
})
export class DynamicsComponent implements OnInit {

  MyFormDynamic: FormGroup = this.fb.group({
    name_person: ['', [Validators.required, Validators.minLength(3)]],
    favorites: this.fb.array([
      ['Metal Gear', Validators.required],
      ['Death Stranding', Validators.required]], Validators.required)
  });

  name_favorite: FormControl = this.fb.control('', [Validators.required, Validators.minLength(3)]);

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {

  }

  get favorites(): any {
    return this.MyFormDynamic.get('favorites') as FormArray;
  }

  addNewFavorite(): void {
    if (this.name_favorite.invalid) {
      return;
    }

    /*((this.MyFormDynamic.controls.favorites) as FormArray).push([])*/
    /*this.favorites.push(new FormControl(this.name_favorite.value, Validators.required));*/
    this.favorites.push(this.fb.control(this.name_favorite.value, [Validators.required]));
    this.name_favorite.reset();

  }

  deleteItem(index: number): void{
    this.favorites.removeAt(index);
  }

  inputValidate(input: string): boolean | null {
    return this.MyFormDynamic?.controls[input]?.errors && this.MyFormDynamic?.controls[input]?.touched;
  }

  saveForm(): void {

    if (this.MyFormDynamic.invalid) {
      this.MyFormDynamic.markAllAsTouched();
      return;
    }
    console.log(this.MyFormDynamic.value);

  }

}
