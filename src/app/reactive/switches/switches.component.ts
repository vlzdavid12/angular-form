import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styles: [
  ]
})
export class SwitchesComponent implements OnInit {

  MyFormSwitch: FormGroup = this.fb.group({
    gender: ['M', Validators.required],
    notify: [false, Validators.required],
    term: [false, Validators.requiredTrue]
  });


  person = {
    gender: 'F',
    notify: true,
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.MyFormSwitch.reset({...this.person, term: false});

    this.MyFormSwitch.get('term')?.valueChanges.subscribe(newValue => console.log(newValue));

    this.MyFormSwitch.valueChanges.subscribe(values => {
      delete values.term;
      this.person = values;
    });
  }

  saveForm(): void{
    const formValue = { ... this.MyFormSwitch.value};
    /*delete formValue.term;*/
    console.log(formValue);

  }

}
