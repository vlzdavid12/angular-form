import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styles: []
})
export class BasicsComponent implements OnInit {

  /* === TODO: FormControl
  MyForm: FormGroup = new FormGroup({
      product : new FormControl('TX 480Ti'),
      price : new FormControl('0'),
      exist : new FormControl('0')
    }); ==== */

  /*=== TODO: FormBuilder ===*/
  MyForm: FormGroup = this.fb.group({
    product: ['', [Validators.required, Validators.minLength(3)]],
    price: ['', [Validators.required, Validators.min(0)]],
    exist: ['', Validators.required]
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    /*-- or reset --*/
    this.MyForm.setValue({
      product: 'RTX 480TI',
      price: 10,
      exist: 4
    });
  }

  inputValidate(input: string): boolean | null{
    return this.MyForm.controls[input].errors && this.MyForm.controls[input].touched;
  }
  saveForm(): void{
    if (this.MyForm.invalid){
      this.MyForm.markAllAsTouched();
      return;
    }
    console.log(this.MyForm.value);
    this.MyForm.reset();
  }

}
