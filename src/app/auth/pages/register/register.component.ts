import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
/*import {namePattern, emailPattern, nameGetStrider} from './../../../shared/validator/validations';*/
import {ValidatorService} from '../../../shared/validator/validator.service';
import {ValidationEmailService} from '../../../shared/validator/validation-email.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent implements OnInit {
  
  miForm: FormGroup = this.fb.group({
    name: ['', [Validators.required, Validators.pattern(this.validatorService.namePattern)]],
    email: ['', [Validators.required, Validators.pattern(this.validatorService.emailPattern)], [this.emailValidatorService]],
    username: ['', [Validators.required, this.validatorService.nameGetStrider]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    password2: ['', [Validators.required]]
  }, {
    validators:  [this.validatorService.inputEquals('password', 'password2')]
  });

  constructor(private fb: FormBuilder,
              private validatorService: ValidatorService,
              private emailValidatorService: ValidationEmailService) { }

  ngOnInit(): void {
    this.miForm.reset({
      name: 'David Valenzuela',
      email: 'test1@test.com',
      username: 'vlzdavid12',
      password: '123456',
      password2: '123456'
    });
  }

  get emailErrorMsg(): string{
    const errors = this.miForm.get('email')?.errors;
    if (errors?.required){
      return 'This input email required';
    } else if (errors?.pattern){
      return 'This format email invalid.';
    } else if (errors?.getEmail){
      return 'This format email it was already taken.';
    }
    return '';
  }

  inputInvalid(input: string): any{
    return this.miForm.get(input)?.invalid && this.miForm.get(input)?.touched;
  }

  sendForm(): void{
    this.miForm.markAllAsTouched();
  }

}
